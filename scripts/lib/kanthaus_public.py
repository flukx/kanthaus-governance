from urllib import request
from datetime import datetime, timedelta

from lib.base import FormattingError, MissingValueError

WATER_URL = "https://cloud.kanthaus.online/s/Hd2ZzF3LjecB93L/download"
ELECTRICITY_URL = "https://cloud.kanthaus.online/s/9tnMHCjnHGf85bo/download"
GAS_URL = "https://cloud.kanthaus.online/s/ptdxb2GTj2psmWY/download"


def read_from_url(url):
    with request.urlopen(url) as response:
        return response.read().decode()


def read_resource_usage_csv(lines):
    usage = {}
    for index, line in enumerate(lines.split('\n')):
        line_num = index + 1
        if line.strip().startswith('#') or len(line) == 0:
            continue

        fields = line.strip().split(',')

        date = fields[0]
        value = fields[1]

        try:
            date = datetime.strptime(date, '%Y-%m-%d')
        except ValueError:
            raise FormattingError('Invalid date format [{}] on line {}'.format(date, line_num))

        if len(value.strip()) == 0:
            # If we have an empty value, replace it with NaN
            value = 'nan'

        try:
            value = float(value)
        except ValueError:
            raise FormattingError('Invalid number format [{}] on line {}'.format(value, line_num))

        usage[date] = value

    return usage


def lookup_values(value_dict, days, offset):
    now = datetime.now()
    today = datetime(year=now.year, month=now.month, day=now.day)
    end = today - timedelta(days=offset)
    start = end - timedelta(days=days)

    values = []
    for date in (start, end):
        try:
            value = value_dict[date]
        except KeyError:
            raise MissingValueError('Requested date {} not found'.format(date))
        else:
            values.append(value)

    return values


def get_from_url(url):
    try:
        usages = read_resource_usage_csv(read_from_url(url))
    except FormattingError as e:
        raise FormattingError('{} - check {}'.format(str(e), url))
    return usages


def get_usage_diff(usages, days, offset):
    try:
        values = lookup_values(usages, days, offset)
    except MissingValueError as e:
        raise MissingValueError('{}'.format(str(e)))

    return values[1] - values[0]


def get_water_usage_data():
    return get_from_url(WATER_URL)


def get_electricity_usage_data():
    return get_from_url(ELECTRICITY_URL)


def get_gas_usage_data():
    return get_from_url(GAS_URL)


def get_water_usage(days=7, offset=0):
    return get_usage_diff(get_water_usage_data(), days, offset)


def get_electricity_usage(days=7, offset=0):
    return get_usage_diff(get_electricity_usage_data(), days, offset)    


def get_gas_usage(days=7, offset=0):
    return get_usage_diff(get_gas_usage_data(), days, offset)
