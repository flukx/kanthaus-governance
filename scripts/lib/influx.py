import http.client
import json
from urllib.parse import urlparse, quote


def influxquery(query):
    url = urlparse(
        "https://grafana.yunity.org/influx-proxy/query?db=kanthaus&q=" + quote(query) + "&epoch=ms"
    )
    connection = http.client.HTTPSConnection(url.netloc)
    connection.request("GET", url.path + '?' + url.query)
    response = connection.getresponse()
    return json.load(response)


def get_electricity_usage_absolute(lower, upper):
    res = influxquery(
        """
            SELECT (last("value") - first("value")) * 0.1
            FROM "homeautomation.Obis"
            WHERE time >= '{}Z' AND time <= '{}Z' AND "node_id" = '4' AND "code"='1.8.0'
        """.format(lower, upper)
    )

    return res["results"][0]["series"][0]["values"][0][1]    


def get_electricity_usage(days=7, offset=0):
    res = influxquery(
        """
            SELECT (last("value") - first("value")) * 0.1
            FROM "homeautomation.Obis"
            WHERE time >= now() - {}d AND time <= now() - {}d AND "node_id" = '4' AND "code"='1.8.0'
        """.format(days + offset, offset)
    )

    return res["results"][0]["series"][0]["values"][0][1]


def get_gas_usage(days=7, offset=0):
    res = influxquery(
        """
            SELECT sum("burner_duration") / 1000
            FROM (
                SELECT elapsed("burner_actually_firing", 1s) as "burner_duration"
                FROM "homeautomation.HeaterStatus"
                WHERE "burner_actually_firing" = False
                AND time >= now() - {}d AND time <= now() - {}d
            )
            WHERE "burner_duration" > 30
        """.format(days + offset, offset)
    )

    return res["results"][0]["series"][0]["values"][0][1]


def get_outdoor_temperature(days=7, offset=0):
    res = influxquery(
        """
            SELECT min("temperature") / 100, mean("temperature") / 100, max("temperature") / 100
            FROM "homeautomation.Environment"
            WHERE ("node_id" = '5')
            AND time >= now() - {}d AND time <= now() - {}d
        """.format(days + offset, offset)
    )

    return res["results"][0]["series"][0]["values"][0][1:4]