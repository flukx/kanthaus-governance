from collections import defaultdict

import csv
from datetime import datetime, timedelta
from os.path import normpath, join, dirname, abspath

from lib.base import FormattingError
from lib.utils import format_date, unidecode
from lib.kanthaus_private import get_residence_file

# base of the kanthaus-private repo
BASE_DIR = normpath(join(dirname(abspath(__file__)), '..', '..'))

RESIDENCE_FILENAME = 'kanthaus-private:residenceRecord.csv'


def get_residence_data():
    """Returns a dict with the person name as key and a list of dates for the value"""
    return process_reader(csv.reader(get_residence_file().splitlines(), delimiter=',', quotechar='"'))


def process_reader(reader):
    """Create list of dates for all names apparent in reader.

    Arguments:
        reader:
            an iterable whose first entry (header) is ignored and all subsequent entries have to elements
            the first element must be a date, the second a comma-separeted list of names without spaces
            usually the reader is a csv file reading object, yielding one entry per line           

    Returns:
        dictionary with names as keys and lists of dates as values
    """
    
    not_too_far_in_the_future = datetime.now() + timedelta(days=5)
    people = defaultdict(list)
    existing_normalized_names = defaultdict(set)
    next(reader)  # skip headers

    def check_similar_names(name):
        """Raise Exception if there are already similar names registered (e.g. only differing in case or umlauts)"""
        normalized_name = unidecode(name.lower())
        name_variations = existing_normalized_names[normalized_name]
        name_variations.add(name)
        if len(name_variations) > 1:
            raise FormattingError(
                'Please do not have confusingly similar names! [{}]'.format(', '.join(sorted(name_variations)))
            )

    for date, names in reader:

        try:

            if names == '':
                continue
            names = [name.strip() for name in names.split(',')]

            for name in names:
                if name == '':
                    raise FormattingError('No empty names please!')
                elif ' ' in name:
                    raise FormattingError('No spaces in names please [{}]'.format(name))
                check_similar_names(name)

            try:
                date = datetime.strptime(date, '%Y-%m-%d')
            except ValueError:
                raise FormattingError('Invalid date format [{}]'.format(date))
            if date > not_too_far_in_the_future:
                raise FormattingError('Woah are you crazy, [{}] is too far into the future'.format(format_date(date)))
            for name in names:
                people[name].append(date)
        except FormattingError as e:
            raise FormattingError('{} - check {} on line {}'.format(str(e), RESIDENCE_FILENAME, reader.line_num))

    return {name: sorted(dates) for name, dates in people.items()}
