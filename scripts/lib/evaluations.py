from collections import defaultdict, namedtuple

import csv
from datetime import datetime
from os.path import join

from lib.base import BASE_DIR, Position, FormattingError

from lib.kanthaus_private import get_evaluation_file


EVALUATIONS_FILENAME = 'kanthaus-private:evaluationRecord.csv'

FIELD_NAMES = [
    'Date',
    'Person',
    'PreviousPosition',
    'AppliedFor',
    'NewPosition',
]

Evaluation = namedtuple('Evaluation', FIELD_NAMES)


def get_evaluation_data():
    """Returns a dict with the person name as the key and list of evaluations
    
    Evaluations are namedtuple objects that use the csv headers
    in evaluationRecord.csv as field names.

    Currently assumed to be ordered in the csv file.
    """
    
    return process_reader(csv.reader(get_evaluation_file().splitlines(), delimiter=',', quotechar='"'))


def process_reader(reader):
    try:
        headers = next(reader)
        missing_headers = set(FIELD_NAMES) - set(headers)
        if len(missing_headers) > 0:
            raise FormattingError('Missing required columns [{}]'.format(','.join(missing_headers)))
        evaluations = defaultdict(list)
        for row in reader:
            if len(row) == 0:
                continue
            obj = {}
            for idx, val in enumerate(row):
                header = headers[idx]
                obj[header] = parse_evaluation_column(header, val)
            try:
                values = [obj[field] for field in FIELD_NAMES]
            except KeyError as e:
                raise FormattingError(
                    '{}: {} check {} on line {}'.format(
                        type(e).__name__,
                        str(e),
                        EVALUATIONS_FILENAME,
                        reader.line_num,
                    )
                )
            evaluation = Evaluation(*values)
            evaluations[evaluation.Person].append(evaluation)
        return evaluations
    except FormattingError as e:
        raise FormattingError(
            '{}: {} check {} on line {}'.format(
                type(e).__name__,
                str(e),
                EVALUATIONS_FILENAME,
                reader.line_num,
            )
        )


def parse_evaluation_column(name, value):
    """Take a raw column in the evaluation table and maybe turn it into something more useful"""
    if name in {'PreviousPosition', 'AppliedFor', 'NewPosition'}:
        return Position(value)
    elif name == 'Date':
        date = datetime.strptime(value, '%Y-%m-%d')
        if date > datetime.now():
            raise FormattingError('Woah, are you crazy! [{}] is in the future'.format(date))
        return date
    return value
