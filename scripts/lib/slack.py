import http.client
import json
from urllib.parse import urlparse
from lib.kanthaus_private import credentials

WEBHOOK = credentials['slack_webhooks']['residence_notifier']


def post(data):
    url = urlparse(WEBHOOK)
    body = json.dumps(data)
    headers = {"Content-type": "application/json"}
    connection = http.client.HTTPSConnection(url.netloc)
    connection.request("POST", url.path, body, headers)
    response = connection.getresponse()
    return response.read()


def notify(message):
    post({'text': message})
