from collections import namedtuple

from enum import Enum
from os.path import normpath, join, dirname, abspath

# base of the kanthaus-private repo
BASE_DIR = normpath(join(dirname(abspath(__file__)), '..', '..'))


class FormattingError(Exception):
    pass


class MissingValueError(Exception):
    pass


class Position(Enum):
    NOTHING = ''
    VISITOR = 'Visitor'
    VOLUNTEER = 'Volunteer'
    MEMBER = 'Member'
    DEPENDENT = 'Dependent'


class EvaluationType(Enum):
    DAYS_VISITED = 'Days Visited'
    ABSENCE = 'Absence'
    ABSOLUTE_DAYS = 'Absolute Days'


Person = namedtuple('Person', ['name', 'dates', 'visits', 'evaluations', 'meta'])
PositionMeta = namedtuple('PositionMeta', ['position', 'reasons'])
EvaluationCriteria = namedtuple('EvaluationCriteria', ['type', 'value', 'threshold', 'required'])
