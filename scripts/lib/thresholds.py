from math import inf

from .base import Position

# max allowable days in house since last evaluation
DAYS_VISITED = {
    Position.NOTHING: inf,
    Position.VISITOR: 21,
    Position.VOLUNTEER: inf,
    Position.MEMBER: inf,
    Position.DEPENDENT: inf,
}

# max allowable days away from house since last evaluation
ABSENCE = {
    Position.NOTHING: inf,
    Position.VISITOR: inf,
    Position.VOLUNTEER: 90,
    Position.MEMBER: 180,
    Position.DEPENDENT: inf,
}

# max days passed since last evaluation
ABSOLUTE_DAYS = {
    Position.NOTHING: inf,
    Position.VISITOR: inf,
    Position.VOLUNTEER: 60,
    Position.MEMBER: 180,
    Position.DEPENDENT: inf,
}
