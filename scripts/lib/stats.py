from datetime import datetime, timedelta
from lib.influx import get_electricity_usage, get_outdoor_temperature
from lib.kanthaus_public import get_water_usage, get_gas_usage, get_electricity_usage as get_electricity_usage_from_git
from lib.base import MissingValueError
import subprocess
import json
import time
from lib.kanthaus_private import credentials

ELECTRICITY_PRICE = 0.2920  # € / kWh
GAS_PRICE = 0.594  # € / m³
WATER_PRICE = 4.5  # € / m³


def get_usage_graph_link():
    time_min = datetime.now() - timedelta(days=30*3)
    time_max = datetime.now()

    subprocess.run([
        "curl",
        "-s",
        "-H",
        "Authorization: Bearer {}".format(credentials['grafana_token']),
        "--output",
        "/tmp/graph.png",
        "https://grafana.yunity.org/render/d-solo/QFAKjLPWk/coordination-meeting-rendering?orgId=4&from={}000&to={}000&theme=light&panelId=2&width=600&height=300&tz=Europe%2FBerlin".format(
            time_min.strftime('%s'),
            time_max.strftime('%s')
        ),
    ])
    res = subprocess.run([
        "curl",
        "-s",
        "https://codi.kanthaus.online/uploadimage",
        "-F"
        "image=@/tmp/graph.png"
    ], capture_output=True)
    d = json.loads(res.stdout.decode('utf8'))
    return d['link']


def get_stats(people, days=7, offset=0):

    time_min = datetime.now() - timedelta(days=days + offset)
    time_max = datetime.now() - timedelta(days=offset)
    spentnights = 0
    for person in people:
        for date in person.dates:
            if date >= time_min and date < time_max:
                spentnights += 1

    try:
        electricity_usage = get_electricity_usage(days=days, offset=offset) / 1000
    except KeyError:
        try:
            electricity_usage = get_electricity_usage_from_git(days=days, offset=offset)
        except MissingValueError:
            electricity_usage = 0
    electricity_cost = electricity_usage * ELECTRICITY_PRICE

    try:
        gas_usage = get_gas_usage(days=days, offset=offset)
    except MissingValueError:
        gas_usage = 0
    gas_cost = gas_usage * GAS_PRICE
    
    try:
        water_usage = get_water_usage(days=days, offset=offset)
    except MissingValueError as e:
        print(e)
        water_usage = 0
    water_cost = water_usage * WATER_PRICE

    try:
        outdoor_temperature = get_outdoor_temperature(days=days, offset=offset)
    except KeyError:
        outdoor_temperature = None

    return {
        "spentnights": spentnights,
        "outdoor_temperature": outdoor_temperature,
        "electricity_usage": electricity_usage,
        "electricity_cost": electricity_cost,
        "gas_usage": gas_usage,
        "gas_cost": gas_cost,
        "water_usage": water_usage,
        "water_cost": water_cost,
    }
