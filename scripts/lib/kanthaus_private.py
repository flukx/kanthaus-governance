import os
import yaml
import subprocess

def get_file(file):
    ps = subprocess.Popen("git archive --remote=git@gitlab.com:kanthaus/kanthaus-private.git HEAD {} | tar -xOf -".format(file).strip(), shell=True, stdout=subprocess.PIPE)
    return ps.stdout.read().decode('utf-8')

def get_credentials():
    return yaml.load(get_file("credentials.yaml"), Loader=yaml.Loader)
    

credentials = get_credentials()

def get_evaluation_file():
    return get_file("evaluationRecord.csv")

def get_residence_file():
    return get_file("residenceRecord.csv")