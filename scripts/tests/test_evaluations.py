import csv
import re
import unittest

from lib.base import Position
from lib.evaluations import process_reader
from lib.utils import format_date


class TestEvaluations(unittest.TestCase):

    def generate_basic(self):
        return csv.reader(
            [
                'Date,Person,PreviousPosition,AppliedFor,NewPosition',
                '2017-02-13,Peter,Visitor,Volunteer,Volunteer',
            ]
        )

    def test_has_one_entry_per_name(self):
        reader = self.generate_basic()
        data = process_reader(reader)
        self.assertEqual(data.keys(), {'Peter'})

    def test_has_a_list_of_evalutions(self):
        reader = self.generate_basic()
        data = process_reader(reader)
        self.assertEqual(len(data['Peter']), 1)
        entry = data['Peter'][0]
        self.assertEqual(format_date(entry.Date), '2017-02-13')
        self.assertEqual(entry.Person, 'Peter')
        self.assertEqual(entry.PreviousPosition, Position.VISITOR)
        self.assertEqual(entry.AppliedFor, Position.VOLUNTEER)
        self.assertEqual(entry.NewPosition, Position.VOLUNTEER)

    def test_complains_about_invalid_positions(self):
        reader = csv.reader(
            [
                'Date,Person,PreviousPosition,AppliedFor,NewPosition',
                '2017-02-13,Peter,ThisIsNotAnActualPosition,Volunteer,Volunteer',
            ]
        )
        with self.assertRaisesRegex(
            Exception,
            re.escape("'ThisIsNotAnActualPosition' is not a valid Position"),
        ):
            process_reader(reader)
