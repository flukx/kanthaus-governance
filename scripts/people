#!/usr/bin/env python3

import argparse
import sys
from math import inf
from datetime import timedelta

from lib.base import FormattingError
from lib.colors import bold, fail, ok, underline, warning, faded
from lib.filters import filter_by_name, filter_by_recent, filter_by_evaluation, evaluation_is_soon
from lib.people import get_people
from lib.utils import format_date

SOON_DAYS = 7
RECENT_DAYS = 7

parser = argparse.ArgumentParser(description='Explore kanthaus data')
parser.add_argument(
    '-e',
    '--evaluation-required',
    action='store_true',
    help='evaluation is due',
)
parser.add_argument(
    '-s',
    '--evaluation-required-soon',
    action='store_true',
    help='evaluation is due within the next {} days'.format(SOON_DAYS),
)
parser.add_argument(
    '-r',
    '--recent',
    action='store_true',
    help='only show people who have been in the house within the last {} days'.format(RECENT_DAYS),
)
parser.add_argument(
    'filter',
    nargs='*',
    help='filter by name(s)',
)
parser.add_argument(
    '--sort-by',
    default='name',
    help='sort list by name or nights',
)
args = parser.parse_args()

try:
    people = get_people()
except FormattingError as e:
    print(str(e), file=sys.stderr)
    sys.exit(1)

if args.sort_by == 'nights':
    people = sorted(people, key=lambda person: len(person.dates))

if args.evaluation_required or args.evaluation_required_soon:
    people = filter(
        filter_by_evaluation(due=args.evaluation_required, soon=args.evaluation_required_soon, soon_days=SOON_DAYS),
        people
    )

if args.recent:
    people = filter(filter_by_recent(RECENT_DAYS), people)

if args.filter:
    people = filter(filter_by_name(args.filter), people)

for person in people:
    print(bold(person.name), ok(person.meta.position.value))
    for reason in person.meta.reasons:
        formatted = '{} {}/{}'.format(reason.type.value, reason.value, reason.threshold)
        if reason.required:
            print('  ', underline('evaluate'), fail(formatted))
        elif evaluation_is_soon(reason, SOON_DAYS):
            print('  ', underline('evaluate'), warning(formatted))
        elif reason.threshold is not inf:
            print('  ', underline('evaluate'), faded(formatted))

    print('  ', underline('nights'), len(person.dates))

    if len(person.visits) > 0:
        print('  ', underline('visits'))
        for start_date, end_date in person.visits:
            print(
                '    ', format_date(start_date - timedelta(days=1)), '-', (end_date - start_date).days + 1, 'nights',
                '-', format_date(end_date)
            )

    if len(person.evaluations) > 0:
        print('  ', underline('evaluations'))
        for evaluation in person.evaluations:
            print(
                '    ',
                format_date(evaluation.Date),
                '-',
                evaluation.PreviousPosition.value,
                evaluation.AppliedFor.value,
                evaluation.NewPosition.value,
            )

    print()
