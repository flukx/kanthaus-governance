---
title: Evaluation facilitation advice
date: 2019-04-20
---

# Pre-evaluation
Please meet to talk with your `<Evaluee>` as early as possible after the evaluation has been scheduled. If they're nervous, try to put them at ease: we all get evaluated here, it's normal, and it helps maintain the community we enjoy.

Help the person introspect about their being at Kanthaus. What do they like? What don't they like? Are they getting the Does anyone have issues with them you can bring up with discretion? Perhaps they have some complicated points which require them to prepare before the evaluation.

You should help the person decide which position they wish to be evaluated for: the decision is theirs. Communicate this position to all the other residents as soon as possible after your chat (e.g. by messaging in #kanthaus)

If you find out during your talk that there are some people it would be beneficial to attend the evaluation (e.g. an interpersonal issue) you can discuss with the `<Evaluee>` how to proceed. Are they able to invite them personally? Would it be better for you to invite them?

# Evaluation

## 1. Preparation
- Ask people to put phones on silent or turn off and generally avoid distractions.
- Use the door indicator when ready to start: it's OK to start without late-comers.
  
## 2. Open part
All current residents are allowed to attend this section.

### 2.1 Introduction
_"`<Evaluee>` has been at Kanthaus for over—_
- _"3 weeks as a Visitor."_ **or**,
- _"2 months as a Volunteer."_ **or**,
- _"6 months as a Member."_

_"`<Evaluee>` is proposing to go forward as a `<Position>` which would allow them to `<Short description of position properties>`

_"We are gathered here to reflect on the relationship between Kanthaus and `<Evaluee>` and consider how it continues; we do this in good faith, with the intention of helping everyone achieve their greatest potential."_

### 2.2. Reflection
_"I invite you all to share a moment of personal reflection."_ (Bell/gong, count at least 5 deep, calm breaths.)

### 2.3. Questions to `<Evaluee>`
1. _"How are you feeling just now?"_
1. _"What have you been doing during this last period and how do feel about it?"_
1. _"Is there anything else you'd like to say going forward?"_

### 2.4. Discussion
As far as possible, keep the discussion about people, projects, problems and positive points _not_ positions. You may want to use an alarm/watch to monitor discussion time: everything cannot be said in one meeting, and it's important evaluations are not exhausting.
1. _"Are there any questions to or from `<Evaluee>`?"_
1. _"Are there any concerns to or from `<Evaluee>`?"_
1. _"Is there anything else to be said before we close the discussion?"_

### 2.5. Closing round
_"Now we'll have a closing round for people to express appreciation. It's fine to say nothing and pass on. Would `<person left of Evaluee>` like to start?"_

(Round happens)

_"Thank you everyone. I now ask `<Evaluee>` and any Visitors_ (and Volunteers in the case `<Evaluee>` wants to become a Member) _to leave the room."_

## 3. Closed part
Only those able to vote are allowed to attend this section.

### 3.1. Mock vote
Done **before** any discussion takes place! Deal every person one set of voting cards. Take a secret vote on the question:

_"Do you support, accept or oppose `<Evaluee>` becoming/remaining a `<Position>`?"_

### 3.2. Discussion
Start a round left to the dealer. Discussion may continue in whatever way seems appropriate.

### 3.3. Final vote
_"Do you support, accept or oppose `<Evaluee>` becoming/remaining a `<Position>`?"_

### 3.4. Finishing tasks
Please ensure they happen, whether you do them or someone else!
- Ventilate room.
- Tidy room.
- Tell `<Evaluee>` the outcome and relevant point brought up in private discussion.
- Create evaluation record. (https://gitlab.com/kanthaus/kanthaus-private/-/blob/master/evaluationRecord.csv)
